import org.jbox2d.collision.shapes.ChainShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;

import java.util.ArrayList;

/**
 * Created by vansl on 4/06/2017.
 * https://github.com/shiffman/The-Nature-of-Code-Examples/blob/master/chp05_physicslibraries/box2d/Exercise_5_3_NoiseChain/Surface.pde
 */
public class Surface {
    ArrayList<Vec2> surface;
    Physics p;
    ChainShape chain;

    public Surface(Physics p) {
        this.p = p;
        this.surface = new ArrayList<Vec2>();

        this.chain= new ChainShape();

        this.addSurfaceVertices();
        this.makeBody();
    }

    private void makeBody() {
        Vec2[] vertices = new Vec2[surface.size()];
        for (int i = 0; i < this.surface.size(); i++) {
            Vec2 point = this.surface.get(i);
            Vec2 edge = this.p.box2d.coordPixelsToWorld(point);
            vertices[i] = edge;
        }

        chain.createChain(vertices, vertices.length);

        BodyDef bd = new BodyDef();
        //bd.position.set(p.box2d.coordPixelsToWorld(0.0f, 0.0f));
        bd.position.set(0.0f, 0.0f);
        Body body = p.box2d.createBody(bd);
        body.createFixture(chain, 1);
    }

    void display() {
        p.strokeWeight(2);
        p.stroke(0);
        p.noFill();
        p.beginShape();
        for(Vec2 v: this.surface) {
            p.vertex(v.x, v.y);
        }
        p.endShape();
    }

    public void addSurfaceVertices() {
        //perlinNoise
        float xoff = 0.0f;

        for (float x = this.p.width+10; x > -10; x -= 5) {
            float y;
            if (x > p.width / 2) {
                y = 50 + (p.width -x) * 1.1f + p.map(p.noise(xoff), 0, 1, -80, 80);
            } else {
                y = 50 + x*1.1f + p.map(p.noise(xoff), 0, 1, -40, 40);
            }

            this.surface.add(new Vec2(x, y));

            xoff += 0.1;
        }
    }
}
