import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by vansl on 31/05/2017.
 */
public class CollectionsApp {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>();

        ArrayList<Link> c = new ArrayList<Link>();

        c.add(new Link("item1"));
        c.add(new Link("item 2"));
        c.add(new Link("david"));
        c.add(new Link("david"));


        List<Link> filtered = c.stream().collect(Collectors.toList());
        System.out.println(filtered);

        Collections.sort(c, new Comparator<Link>() {
            @Override
            public int compare(Link o1, Link o2) {
                return o1.title.compareTo(o2.title);
            }
        });

        int freq = Collections.frequency(filtered, new Link("david"));
        System.out.format("Frequency of david in the collection is: %dn",  freq);

        c.stream()

                .forEach(e -> System.out.println(e.title));
    }
}