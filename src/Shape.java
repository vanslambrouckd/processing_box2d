/**
 * Created by vansl on 31/05/2017.
 */
public abstract class Shape {
    public abstract void draw(Canvas c);
}
