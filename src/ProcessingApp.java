/**
 * Created by vansl on 31/05/2017.
 */
import processing.core.PApplet;
import org.jsoup.*;
import processing.core.PImage;

public class ProcessingApp extends PApplet {
    PImage img;

    public static void main(String[] args) {
        PApplet.main("ProcessingApp", args);
    }

    public void setup() {
        frameRate(60);
        background(255);
        img = loadImage("frog.jpg");
    }

    public void drawImage() {
        loadPixels();
        img.loadPixels();

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int loc = x + y * width;
                float r = red(img.pixels[loc]);
                float g = green(img.pixels[loc]);
                float b = blue(img.pixels[loc]);

                float distance = dist(x, y, mouseX, mouseY);
                //float adjustBrightness = (50-distance)/50;
                float adjustBrightness = 1;
                if (loc < 60000) {
                    adjustBrightness = 0.2f;
                }
                //float adjustBrightness = ((float)mouseX/width)*8.0f;
                r*= adjustBrightness;
                g*= adjustBrightness;
                b*= adjustBrightness;
                r = constrain(r, 0, 255);
                g = constrain(g, 0, 255);
                b = constrain(b, 0, 255);
                //r = brightness((int)r+5);

                int color = color(r, g, b);
                pixels[loc] = color;
                //pixels[loc] = img.pixels[loc];
            }
        }
        updatePixels();}

    public void test() {
        stroke(0);
        fill(255,0,0);
        //ellipse(width/2,height/2,100,100);
        float d = dist(0, 0, mouseX, mouseY);
        System.out.println(mouseX);

        loadPixels();
        int loc = mouseX+mouseY*width;
        int color = color(255,0,0);
        int brushSize = 20;

        if ((mouseX > brushSize) && (mouseX < (width-brushSize))) {
            for (int x = 0; x < 5; x++) {
                pixels[loc-x] = color;
                pixels[loc+x] = color;
            }
        }

        pixels[loc] = color;
        updatePixels();

    }

    public void draw() {
        fill(255,0,0);
        //System.out.println(adjustBrightness);
        //this.drawImage();
    }

    public void settings() {
        size(500,334);
    }
}
