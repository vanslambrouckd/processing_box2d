/**
 * Created by vansl on 4/06/2017.
 * http://natureofcode.com/book/chapter-5-physics-libraries/
 * https://github.com/shiffman/The-Nature-of-Code-Examples/blob/master/chp05_physicslibraries/box2d/NOC_5_1_box2d_exercise/NOC_5_1_box2d_exercise.pde
 */

import org.jbox2d.common.Vec2;
import processing.core.PApplet;
import shiffman.box2d.Box2DProcessing;

import java.util.ArrayList;


public class Physics extends PApplet {
    public Box2DProcessing box2d;

    ArrayList<Box> boxes;
    ArrayList<Boundary> boundaries;
    Surface surface;

    public static void main(String[] args) {
        PApplet.main("Physics", args);
    }

    public void setup() {
        fill(255,255,0);

        box2d = new Box2DProcessing(this);
        box2d.createWorld();

        Vec2 gravity = new Vec2(2.5f, -10f);
        box2d.setGravity(gravity.x, gravity.y);

        boxes = new ArrayList<Box>();
        boundaries = new ArrayList<Boundary>();

        /*
        boundaries.add(new Boundary(this, width/4, height-5, width/2-50, 10));
        boundaries.add(new Boundary(this, 3*width/4,height-50,width/2-50,10));
        */

        surface = new Surface(this);
    }

    public void settings() {
        size(640,360);
    }

    public void draw() {
        background(255);
        box2d.step();

        if (random(1) < 0.2) {
            Box p = new Box(this, width/2, 30);
            boxes.add(p);
        }

        if (mousePressed) {
            Box p = new Box(this, mouseX, mouseY);

            boxes.add(p);
        }

        for(Box b: boxes) {
            b.display();
        }

        for(Boundary wall: boundaries) {
            wall.display();
        }

        surface.display();

        String msg = "nr boxes: " + this.boxes.size();
        fill(255,0,0);
        text(msg, 10, 10);
        for (int i = boxes.size()-1; i >= 0; i--) {
            Box b = boxes.get(i);
            if (b.done()) {
                boxes.remove(i);
            }
        }

    }
}
