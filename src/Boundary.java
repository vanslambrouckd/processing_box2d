/**
 * Created by vansl on 4/06/2017.
 */

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;

public class Boundary {
    float x, y;
    float w,h;

    Physics p;
    Body b;

    public Boundary(Physics p, float x, float y, float w, float h) {
        this.p = p;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;

        BodyDef bd = new BodyDef();
        bd.position.set(this.p.box2d.coordPixelsToWorld(x, y));
        bd.type = BodyType.STATIC;
        b = this.p.box2d.createBody(bd);

        float box2dW = this.p.box2d.scalarPixelsToWorld(w/2);
        float box2dH = this.p.box2d.scalarPixelsToWorld(h/2);

        PolygonShape ps =new PolygonShape();
        ps.setAsBox(box2dW, box2dH);

        b.createFixture(ps, 1);
    }

    void display() {
        this.p.fill(0);
        this.p.stroke(0);
        this.p.rectMode(this.p.CENTER);
        this.p.rect(x, y, w, h);
    }
}
