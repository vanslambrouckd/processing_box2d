/**
 * Created by vansl on 4/06/2017.
 */

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.*;
import org.jbox2d.dynamics.FixtureDef;
import processing.core.PApplet;
import shiffman.box2d.Box2DProcessing;

public class Box {
    //https://processing.org/tutorials/eclipse/
    Physics p;
    Body body;

    float x, y;
    float w, h;

    public Box(Physics p, float x, float y) {
        this.p = p;
        this.x = x;
        this.y = y;
        this.w = 16;
        this.h = 16;

        this.makeBody();
    }

    private void makeBody() {
        BodyDef bd = new BodyDef();
        bd.type = BodyType.DYNAMIC;
        bd.position.set(this.p.box2d.coordPixelsToWorld(x, y));

        body = this.p.box2d.createBody(bd);

        PolygonShape ps = new PolygonShape();
        float box2dw = this.p.box2d.scalarPixelsToWorld(w/2);
        float box2dh = this.p.box2d.scalarPixelsToWorld(h/2);

        ps.setAsBox(box2dw, box2dh);

        FixtureDef fd = new FixtureDef();
        fd.shape = ps;
        fd.density = 1;
        fd.friction = 0.3f;
        fd.restitution = 0.5f;

        body.createFixture(fd);
    }

    public void display() {
        Vec2 pos = this.p.box2d.getBodyPixelCoord(body);
        float a = body.getAngle();


        p.pushMatrix();
        p.translate(pos.x, pos.y);
        p.rotate(-a);
        p.fill(127);
        p.stroke(0);
        p.strokeWeight(2);
        p.rectMode(PApplet.CENTER);
        p.rect(0, 0, w, h);
        p.popMatrix();
    }

    // This function removes the particle from the box2d world
    void killBody() {
        this.p.box2d.destroyBody(body);
    }

    boolean done() {
        Vec2 pos = this.p.box2d.getBodyPixelCoord(body);

        if (pos.y > this.p.height) {
            this.killBody();
            return true;
        }
        return false;
    }
}
