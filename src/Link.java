/**
 * Created by vansl on 31/05/2017.
 */
public class Link extends Shape {
    String title;

    public Link(String title) {
        this.title = title;
    }

    @Override
    public void draw(Canvas c) {

    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Link) {
            Link link = (Link) obj;
            return link.title == this.title;
        }

        return false;
    }

    @Override
    public String toString() {
        return this.title;
    }
}
