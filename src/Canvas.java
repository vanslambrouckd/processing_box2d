import java.util.ArrayList;

/**
 * Created by vansl on 31/05/2017.
 */
public class Canvas {
    ArrayList<Shape>shapes = new ArrayList<>();

    Canvas() {

    }

    public void addElement(Shape el) {
        shapes.add(el);
    }

    public void draw() {
        for(Shape sh: shapes) {
            sh.draw(this);
        }
    }
}
